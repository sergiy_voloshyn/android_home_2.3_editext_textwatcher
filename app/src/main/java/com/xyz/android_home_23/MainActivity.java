package com.xyz.android_home_23;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
/*
3*. Расположить на экране два контейнера на каждом по три текстовых поля для ввода цифр, на первом контейнере есть еще и три кнопки.
После ввода цифр в десятичном формате и нажатии соответствующей кнопки в первом контейнере - во втором контейнере соответствующее поле
 заполняется тем же числом, но в другой системе счисления (двоичной, восьмеричной и шестнадцатеричной соответственно).
3.1* Реализовать тоже задание, но без кнопок. Цифры преобразуются автоматически, каждый раз, как только пользователь вводит новые данные
 (изменения отслеживаются с помощью TextWatcher).


 */

public class MainActivity extends AppCompatActivity {

    Button binButton;
    Button octButton;
    Button hexButton;

    EditText inputTextForBin;
    EditText inputTextForOct;
    EditText inputTextForHex;

    TextView displayBin;
    TextView displayOct;
    TextView displayHex;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        binButton = (Button) findViewById(R.id.button_bin);
        octButton = (Button) findViewById(R.id.button_oct);
        hexButton = (Button) findViewById(R.id.button_hex);

        inputTextForBin = (EditText) findViewById(R.id.edit_first);
        inputTextForOct = (EditText) findViewById(R.id.edit_second);
        inputTextForHex = (EditText) findViewById(R.id.edit_third);

        displayBin = (EditText) findViewById(R.id.res_edit_bin);
        displayOct = (EditText) findViewById(R.id.res_edit_oct);
        displayHex = (EditText) findViewById(R.id.res_edit_hex);


        binButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int num = Integer.parseInt(inputTextForBin.getText().toString(), 10);
                displayBin.setText(Integer.toBinaryString(num));
            }
        });
        octButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int num = Integer.parseInt(inputTextForOct.getText().toString(), 10);
                displayOct.setText(Integer.toOctalString(num));
            }
        });
        hexButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int num = Integer.parseInt(inputTextForHex.getText().toString(), 10);
                displayHex.setText(Integer.toHexString(num));
            }
        });


    }
}
